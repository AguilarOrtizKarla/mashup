var count = 0;

function validate(streetStr, cityStr) {    
    var streetStr = $('#street').val();
    var cityStr = $('#city').val();

    if (streetStr.length == 0 && cityStr.length == 0) {
        $('#street')
        .css({
            'border':'1px solid red'
        });

        $('#city')
        .css({
            "border":"1px solid red",
        });
        
        
        $('#cityerr').removeClass('onlycityerror');

        $('#strerr')
        .css({
            "display":"inline"
        })
        .text('Ingrese La calle!!')
        .addClass('streetcityerror');
        
        if(count != 0) {
            $('#strerr').css('color','red');
        }

        $('#cityerr')
        .css({
            "display":"inline"
        })
        .text('Ingrese La Ciudad!')
        .addClass('streetcityerror');
        
        if(count != 0) {
            $('#cityerr').css('color','red');    
        }
            
                
        return false;
    } 
    else if (streetStr.length == 0 && cityStr.length != 0) {
        $('#street').css('border','1px solid red');
        $('#city').css("border","");
        $('#strerr').append("<br>");
        $('#strerr').css("display","inline");
        $('#cityerr').css("display","none");
        $('#strerr').text('Calle Requerida!');
        $('#strerr').addClass('streetcityerror');
        if(count != 0) {
            $('#strerr').css('color','red');            
        }
        return false;
    } 
    else if (streetStr.length != 0 && cityStr.length == 0) {
        $('#city').css("border","1px solid red");
        $('#street').css("border","");
        $('#strerr').css("display","none");
        $('#cityerr').css("display","inline");
        $('#cityerr').text('Ciudad Requerida!');
        $('#cityerr').addClass('onlycityerror');
        if(count != 0) {
            $('#cityerr').css('color','red');            
        }
        return false;
    }
    else {
        $('#street').css("border","");
        $('#city').css("border","");
        $('#strerr').css("display","none");
        $('#cityerr').css("display","none");
        count = 1;
        return true;
    }
}

function loadData() {
    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytCity = $('#nyt-city');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');
    var $weatherHeader = $('#weather-header');
    var $weatherInfo = $('#weather-info');
    $wikiElem.text("");
    $nytElem.text("");
    var streetStr = $('#street').val();
    var cityStr = $('#city').val();
    if (validate(streetStr, cityStr)) {   
        var address = streetStr + ', ' + cityStr;
        $greeting.text('Entonces, Me gustaria vivir en... ' + address + '?');
 
        // cargar vista de calle
        var streetviewUrl = 'http://maps.googleapis.com/maps/api/streetview?size=600x400&location=' + address + '';
        $body.append('<img class="bgimg" src="' + streetviewUrl + '">');

        // cargar noticias
        var nytimesUrl = 'http://api.nytimes.com/svc/search/v2/articlesearch.json?q=' + cityStr + '&sort=newest&api-key=ef59e7aa1625ddc16d9bd7ee12f7ec65:19:73004334';
        $.getJSON(nytimesUrl, function(data){
              var offset = $(".nytimes-container").offset();
                var nycont_ht = window.innerHeight - offset.top - 5;
                $('.nytimes-container').css('height', nycont_ht);
                $(".nytimes-container").css("overflow", "scroll");
            $nytHeaderElem.text('Noticias de la NY TIMES!!').append('<br><p>'+cityStr+'</p>');
            articles = data.response.docs;
            for (var i = 0; i < articles.length; i++) {
                var article = articles[i];
                $nytElem.append('<li class="article">'+
                    '<a href="'+article.web_url+'">'+article.headline.main+'</a>'+
                    '<p>' + article.snippet + '</p>'+ 
                '</li>');
            };
        }).error(function(e){
            $nytHeaderElem.text('NO ENCOTRADO');
        });

        // datos de wiki

        var wikiUrl = 'http://en.wikipedia.org/w/api.php?action=opensearch&search=' + cityStr + '&format=json&callback=wikiCallback';
        var wikiRequestTimeout = setTimeout(function(){
            $wikiElem.text("WIKIPEDIA NO CARGO :v ");
        }, 8000);
    
        $.ajax({
            url: wikiUrl,
            dataType: "jsonp",
            jsonp: "callback",
            success: function( response ) {
                var articleList = response[1];
                for (var i = 0; i < articleList.length; i++) {
                    articleStr = articleList[i];
                    var url = 'http://en.wikipedia.org/wiki/' + articleStr;
                //    $wikiElem.append('<li><a href="' + url + '">' + articleStr + '</a></li>');
                }
                clearTimeout(wikiRequestTimeout);
            }
        });

var texto=$("#city").val();

    if(texto!='')//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $.ajax({
          dataType: 'json',
          url: 'https://api.tumblr.com/v2/tagged?tag='+texto+'&api_key=fuiKNFp9vQFvjLNvx4sUwti4Yb5yGutBN4Xh10LXZhhRKjWlV4',
         beforeSend: function(){
                     console.log("ENVIANDO DATOS A Tumblr...");
          
        },
        success: function(data) {
                    console.log("RECIBIENDO DATOS DE Tumblr...");
                    for (var i =0 ; i < 5 ; i++) {
                    $wikiElem.append("<div class='panel-heading' > <h6> <li> Descripcion: </li> "+ data.response[i].caption+" <li> Nombre del blog :"+data.response[i].blog_name+"</li> </h6> <li>URL:  <a href="+data.response[i].image_permalink+" >"+ data.response[i].image_permalink+"</a></li> <div>");
                         
                     console.log(".- "+data.response[i].blog_name);
                    };
          }
      });
    }
    } 

    return false;
};
function loadMap(){
    var streetStr = $('#street').val();
    var cityStr = $('#city').val();
    
    if (validate(streetStr, cityStr)) {

        var address = streetStr + ', ' + cityStr;
        var gmapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false";

        $.ajax({
            url: gmapUrl,
            type : "POST",
            dataType: "json",
            cache: false,
            success: function(result){
                if(result.results.length){
                    var lat=result.results[0].geometry.location.lat;
                    var lon=result.results[0].geometry.location.lng;     
                    var latlng = new google.maps.LatLng(lat, lon);     
                    var mapOptions = {
                      zoom: 12,
                      center: latlng,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("gmap"),mapOptions);
                    var marker = new google.maps.Marker({
                        position: result.results[0].geometry.location,
                        map: map,
                        title:address
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });
                    $('#gmap').css('height','400px');
                    $('#gmap').css('width','350px');
                }
            }
        });
    } 
}

function updateWeather(cityStr) {

    var $weatherHeader = $('#weather-header');
    var $weatherInfo = $('#weather-info');
    
        var weatherUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + cityStr + "&APPID=2ef9e90f8ea043df6d3ed60bd738a919";

        //callback function from ajax request
        /*setInterval(myTimer, 1000);*/
        $.getJSON( weatherUrl, function(response) {
            $('.weather-container').css('height','200px');
            $('#weathernewinfo').remove();
            $weatherHeader.empty();
            $weatherInfo.empty();
          
            $weatherHeader.text('Clima en  ' + cityStr);
            $weatherHeader.css('color','blue');
            description = response.weather[0].description;
            temp = response.main.temp - 273.15;
            var temp_roundoff = Math.round(temp * 100) / 100;
            resplat = response.coord.lat;
            resplon = response.coord.lon;
            humidity = response.main.humidity;
            if(humidity != "undefined"){
                humidity_perc = humidity + " %";
            }
            else {
                humidity_perc = humidity;
            } 
            wind = Math.round(response.wind.speed * 18 / 5);
            $weatherInfo
            .append(
                '<p id="weathernewinfo"><b>Temperatura: '+temp_roundoff+'&deg;C</b><br>' + 
                '<b>Humedad: '+humidity_perc+'</b><br>' + 
                '<b>Viento: '+wind+' km/h</b><br>' + 
                '<b>Descripcion: '+description+'</b></p>'
            );

        })
        .error(function(e){
            $weatherHeader.text('CLIMA NO CARGO ---');
        });

}

function weather() {
        
    $('#form-container').submit(function() {
        var streetStr = $('#street').val();
        var cityStr = $('#city').val();            
        if (validate(streetStr, cityStr)) {
            setInterval(updateWeather(cityStr), 5000);
        }
    });
         
}      




/*    
    $.getJSON({
        url: gmapUrl,
        success: function(results, status) {
            var mapOptions = {
               	center:new google.maps.LatLng(0,0), 
               	zoom:12,
               	mapTypeControl: true,
	           	mapTypeControlOptions: {
      	    		style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		        },
		        navigationControl: true,
               	mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("gmap"),mapOptions);
        }          
    });
*/

$(document).ready(function() {

    weather();

    $('#form-container').submit(loadData);

    $('#form-container').submit(loadMap);
    

});
$(document).ready(function() {
  $('form').submit(function(event) {
    event.preventDefault();
    var $searchField = $('#search');
    var $submitButton = $('#submit');

    $searchField.prop("disabled", true);
    $submitButton.attr("disabled", true).val("searching...");

    var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
    var animal = $searchField.val();
    var flickerOptions = {
      tags: animal,
      format: "json"
    };

    function displayPhotos(data) {
      var photoHTML = '<p><ul>';
      $.each(data.items, function(i, photo) {
        photoHTML += '<li class="grid-25 tablet-grid-50">';
        photoHTML += '<a href="' + photo.link + '" class="image">';
        photoHTML += '<img src="' + photo.media.m + '"></a></li>';
      });
      photoHTML += '</ul></p>';
      $('#photos').html(photoHTML);
      $searchField.prop("disabled", false);
      $submitButton.attr("disabled", false).val("Search");
    }
    $.getJSON(flickerAPI, flickerOptions, displayPhotos);
  })
});